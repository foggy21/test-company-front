import * as React from 'react';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';

export default function CollapsibleList() {
  const [departments, setDepartments] = React.useState([])
  function Hierarchy({ department }) {
    const [open, setOpen] = React.useState(false);

    const handleClick = () => {
      setOpen(!open);
    };

    return (
        <List component="nav">
          <ListItemButton sx={{ maxWidth: 300, bgcolor: 'background.paper' }}
           onClick={handleClick}>          
            <ListItemText primary={department.name} />
            {open ? <ExpandLess /> : <ExpandMore />}
          </ListItemButton>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <List sx={{textAlign: "start", pl: 4}} component="div" disablePadding>
              <p>Employees:</p>
              {department.employees.map((employee) => (
                
                <p key={employee.id}>{employee.name} - {employee.position}</p>
              ))}
              <List>
              {department.divisions.map((division) => (
                <Hierarchy key={division.id} department={division} />
              ))}
              </List>
            </List>
          </Collapse>
        </List>
    );
}

  React.useEffect(() => {
      fetch("http://localhost:8080/department/hierarchy")
      .then(res=>res.json())
      .then((result)=>{
          setDepartments(result)
          console.log(result)
      })
  }, [])

  return (  
      <List>
      {departments.map((department) => (
          <Hierarchy key={department.id} department={department} />
      ))}
      </List>
  );
}