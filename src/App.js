import { List } from '@material-ui/core';
import './App.css';
import CollapsibleList from './components/CollapsibleList';

function App() {
  return (
    <List sx={{ width: "100%", maxWidth: 300, bgcolor: 'background.paper' }} 
    className="App">
      <CollapsibleList/>
    </List>
  );
}

export default App;
